var express = require("express");
var router = express.Router();
var path = require("path");

router.get("/", function(req, res, next) {
  res.sendFile(path.join(__dirname + "./../public/gonogo.html"));
});

router.get("/blog", function(req, res, next) {
  res.sendFile(path.join(__dirname + "./../public/blog.html"));
});

router.get("/gonogo", (req, res) => {
  res.sendFile(path.resolve("./public/gonogo.apk"));
});

router.use(express.static(__dirname + '/public'));

router.get("/health", (req, res) => {
  res.json({status: "ok"});
});

module.exports = router;
