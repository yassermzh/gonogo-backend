const express = require("express");
const router = express.Router();
const fs = require("fs");
const path = require("path");
const levelup = require("levelup");
const leveldown = require("leveldown");
const {
  head,
  take,
  compose,
  filter,
  map,
  min,
  mean,
  keys,
  values,
  flatten,
  length,
  last
} = require("ramda");

var db = levelup(leveldown("./profiles"));

const getUsers = () => {
  let users = {};
  return new Promise((resolve, reject) => {
    db.createReadStream()
      .on("data", function(data) {
        // console.log(data.key.toString(), "=", data.value.toString());
        users[data.key.toString()] = JSON.parse(data.value.toString());
      })
      .on("error", function(err) {
        console.log("Oh my!", err);
        reject();
      })
      .on("close", function() {
        console.log("Stream closed");
      })
      .on("end", function() {
        console.log("Stream ended");
        resolve(users);
      });
  });
};

router.get("/", (req, res) =>
  getUsers()
    .then(usersSessionsStats)
    .then(data =>
      res.render("session-stats", {
        params: data,
        moment: require("moment-jalaali")
      })
    )
    .catch(
      e =>
        console.error("faile, e=", e) ||
        res.status(500).json({ error: "failed" })
    )
);

router.get("/details", (req, res) =>
  getUsers()
    .then(usersStats)
    .then(data => res.render("stats", { params: data }))
    .catch(
      e =>
        console.error("faile, e=", e) ||
        res.status(500).json({ error: "failed" })
    )
);

router.get("/details-verbose", (req, res) =>
  getUsers()
    .then(data => res.json(data))
    .catch(
      e =>
        console.error("faile, e=", e) ||
        res.status(500).json({ error: "failed" })
    )
);

router.get("/", (req, res) =>
  getUsers()
    // .then(usersStats)
    .then(data => res.json(data))
    // .then(data => res.render("stats", {params: data}))
    .catch(
      e =>
        console.error("faile, e=", e) ||
        res.status(500).json({ error: "failed" })
    )
);

const save = (key, value) => db.put(key, value);

const getProfilePath = req => path.join("profiles", req.headers.token);

router.post("/", (req, res) => {
  const { data } = req.body;
  console.log("req=", req.body);
  console.log("token=", req.headers.token);

  if (!data || !req.headers || !req.headers.token.startsWith("gonogo-"))
    return res.status(401).json({ error: "data format" });
  save(getProfilePath(req), JSON.stringify(data), "utf-8", err => {
    return err
      ? res.status(500).json({ error: "failed to save" })
      : res.json({ status: "ok" });
  });
});

const denormalizeSessionToBlockTrials = game => (session = {}) =>
  (session.blockIDs || [])
    .map(blockID => game.blocks[blockID])
    .map(block => ({
      gameID: block.gameID,
      trials: block.trialIDs.map(trialID => game.trials[trialID])
    }))
    .map(block => {
      return {
        game:
          block.gameID == "hands"
            ? "hands"
            : +block.gameID > 10
              ? "colors"
              : "binary",
        success: compose(
          length,
          filter(trial => trial.response == "SUCCESS")
        )(block.trials),
        omission: compose(
          length,
          filter(trial => trial.response == "OMISSION")
        )(block.trials),
        comission: compose(
          length,
          filter(trial => trial.response == "COMISSION")
        )(block.trials),
        response: compose(
          Math.round,
          mean,
          map(trial => min(trial.rt, 800)),
          filter(trial => trial.rt > 0)
        )(block.trials)
      };
    });

const completedSessionsStats = game =>
  compose(
    map(session => session.finishedAt),
    filter(session => session.completed),
    values
  )(game.sessions) || {};

const gameStats = game =>
  compose(
    // head,
    // flatten,
    // denormalizeSessionToBlockTrials(game),
    map(denormalizeSessionToBlockTrials(game)),
    // head,
    filter(session => session.completed),
    values
  )(game.sessions) || {};

const userStats = fn => user => ({ ...user.profile, stats: fn(user.game) });

const isValidTime = d => d instanceof Date && !isNaN(d);

const usersSessionsStats = users =>
  values(users)
    .filter(user => user.profile.name)
    .map(userStats(completedSessionsStats))
    .map(d => console.log("d=", d) || d)
    .sort((user1, user2) => {
      console.log("t=", new Date(last(user1.stats)));
      const t1 = new Date(last(user1.stats));
      const t2 = new Date(last(user2.stats));
      if (!isValidTime(t2)) return 1000000;
      return t2 - t1;
    });

const usersStats = users =>
  values(users)
    .filter(user => user.profile.name)
    .map(userStats(gameStats));

module.exports = router;
